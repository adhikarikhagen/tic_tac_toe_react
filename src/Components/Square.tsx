import React from "react";
import { Button } from "antd";
import styled from "@emotion/styled";





type SquareProps = {
    value: string;
    winnningSquare:boolean;
    onClick: () => void;
  };

  const Square: React.SFC<SquareProps> = ({ value, onClick,winnningSquare}) => {
    const StyledButton = styled(Button)`
    font-size:4vw;
    width:150px;
    font-weight:bolder;
    height:130px;
    box-sizing:border-box;
    text-align:center;
    color:#b73f2b;
    background-color:${winnningSquare? "#7d8892c4" : "black"};
    :hover{
        background-color:#aeb4d1;
    }
    @media screen and (max-width: 425px) {
      width:30px;
      height:30px;

    }
    
`;
    return <StyledButton  onClick={onClick}>{value}</StyledButton>;
  };

  export default Square