import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Button, Select, Input,message } from "antd";
import Square from "../Components/Square";
import styled from "@emotion/styled";

import {
 WarningOutlined,
 UserOutlined 
} from "@ant-design/icons";


const { Option } = Select;

type UserProps = {
  firstName:string;
  secondName:string;
  weapon:string;
  status: boolean;
};

const StyledStatus = styled.div`
  text-align: center;
  margin-top: 50px;
  padding-bottom:30px;
  color:#38b6c3;
  font-size: 3vw;
`;
const StyledContainer = styled.div`
  padding: 10px;
 
`;
const StyledHeader = styled.h1`
  font-size: 1.5vw;
  padding-top:10px;
  padding-bottom:30px;
  text-align: center;
  color: #001529;
`;
const StyledCol = styled.div`
  cursor: inherit;
`;

const StyledPlayerNames = styled.div` font-size:2vw;  position:absolute; padding-left:50px; top:300px; font-weight:600;`
const StyledName = styled.span`color:#b73f2b`
message.config({
  top: 100,
  duration: 1.5
});

//Calculate winner
const calculateWinner = (squares: any) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return lines[i];
    }
  }
  return null;
};

//Determine if board is full
const isBoardFull = (squares: any) => {
  for (let i = 0; i < squares.length; i++) {
    if (squares[i] == null) {
      return false;
    }
  }
  return true;
};

const User: React.SFC<UserProps> = ({firstName,secondName,weapon,status}) => {
  const [squares, setSquares] = useState(Array(9).fill(null));
  const [isXNext, setIsXNext] = useState(true);
  const winner = calculateWinner(squares);
  let  nextSymbol:any;
  
    if(weapon==='X'){
       nextSymbol = isXNext? "X" : "O";
    }
    else
     nextSymbol = isXNext? "O": "X";


  const getStatus = () => {
    if (winner) {
      const winnerPlayer = isXNext? secondName : firstName
      return "Winner: " + winnerPlayer;
    } else if (isBoardFull(squares)) {
      return "Draw!";
    } else {
      const nextPlayr = isXNext? weapon : weapon==='X'?"O":"X";
      return  "It's " + nextPlayr + " Turn";
    }
  };
  const StyledRow = styled(Row)`
    pointer-events: ${winner || getStatus() === "Draw!" ? "none" : "default"};
  `;
  const renderSquare = (i: number) => {
    let currentSquare = false;
    if (winner) {
      winner.forEach(element => {
        if (element === i) {
          currentSquare = true;
        }
      });
    }
  
    
    return (
      <Square
        winnningSquare={currentSquare}
        value={squares[i]}
        onClick={() => {
          if (squares[i] != null || winner != null) {
            return;
          }
          const nextSquares = squares.slice();
          nextSquares[i] = nextSymbol;
          setSquares(nextSquares);
          setIsXNext(!isXNext);
        }}
      />
    );
  };

  let restartButton = false;
  for (let i = 0; i < squares.length; i++) {
    if (squares[i] != null) {
      restartButton = true;
    }
  }
  console.log(firstName,secondName,weapon);
  return (
    <StyledContainer>
      <StyledHeader>PC GAME
      </StyledHeader>
      <StyledPlayerNames>
      <p>   Player1 : <StyledName>{firstName} ({weapon})</StyledName></p>
       <p>  Player2 : <StyledName>{secondName}</StyledName></p>
      </StyledPlayerNames>
      <StyledRow justify="center">
        <StyledCol>{renderSquare(0)}</StyledCol>
        <StyledCol>{renderSquare(1)}</StyledCol>
        <StyledCol>{renderSquare(2)}</StyledCol>
      </StyledRow>
      <StyledRow justify="center">
        <StyledCol>{renderSquare(3)}</StyledCol>
        <StyledCol>{renderSquare(4)}</StyledCol>
        <StyledCol>{renderSquare(5)}</StyledCol>
      </StyledRow>
      <StyledRow justify="center">
        <StyledCol>{renderSquare(6)}</StyledCol>
        <StyledCol>{renderSquare(7)}</StyledCol>
        <StyledCol>{renderSquare(8)}</StyledCol>
      </StyledRow>
      <StyledStatus>
        {getStatus()}
        {restartButton && (
          <div>
            <Button
              type="primary"
              size="large"
              onClick={() => {
                setSquares(Array(9).fill(null))
                setIsXNext(true);
              }}
            >
              Restart
            </Button>
          
          </div>
        )}
      </StyledStatus>
    </StyledContainer>
  );
};

const StyledPlayButton = styled(Button)`position:absolute; top:50%; left:50%;`
const StyledWarning = styled.div`color:red; margin-top:4px; margin-left:3px;`
const StyledInput = styled(Input)`margin: 4px 0;`
const GameContainer = styled.div`height:740px; color:white; margin-top:25px;background-color:#000000eb;`
const StyledModal = styled(Modal)``
const Game: React.SFC = () => {
  const [firstName, setFirstName] = useState("");
  const [secondName, setSecondName] = useState("")
  const [weapon, setWeapon] = useState("X");
  const [success, setSuccess] = useState(false);
  const [error, setError]  = useState('')

  const [visible, setVisible] = useState(false);
  const submitData = () => {
    if(firstName==='' || secondName===''){
      setError('Enter valid player name')
    }
     else if(firstName===secondName){
      setError('Enter different name')
    }
    
    else if (firstName !==secondName && weapon) {
      setVisible(false);
      setSuccess(true);
    } 
  };

  useEffect(() => {
   

  });

  return (
    <GameContainer>
     {success ? <User firstName={firstName} secondName={secondName} weapon={weapon} status={success}/> : 
     <>
      <StyledPlayButton ghost shape='round'type="primary" size="large" onClick={() => setVisible(true)}>
        Play Game
      </StyledPlayButton>
      <StyledModal
        title="Enter player names"
        visible={visible}
        onOk={() => submitData()}
        onCancel={() => setVisible(false)}
      >
        <Row gutter={18} align='middle'>
          <Col span={18}>
            <StyledInput
            prefix={<UserOutlined />}
              placeholder="Player one name"
              onChange={e => setFirstName(e.target.value)}
            />
          </Col>
          <Col span={3}>
            <Select
              defaultValue="X"
              style={{ width: 120 }}
              onChange={value => setWeapon(value)}
            >
              <Option value="X">X</Option>
              <Option value="O">O</Option>
            </Select>
          </Col>
        </Row>
        <Row gutter={18}>
          <Col span={18}>
            <StyledInput
            prefix={<UserOutlined />}
              placeholder="Player two name"
              onChange={e => setSecondName(e.target.value)}
            />
          </Col>
          <Col span={3}></Col>
        </Row>
        {error && 
        <StyledWarning>
        <WarningOutlined/>{error}</StyledWarning>}

      </StyledModal></> }
    </GameContainer>
  );
};
export default Game;
