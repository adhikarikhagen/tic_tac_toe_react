import React, { useEffect, useState } from "react";
import { Input, Row, Col, Spin, message } from "antd";
import moment from "moment";
import { createFromIconfontCN } from "@ant-design/icons";

import styled from "@emotion/styled";

const axios = require("axios").default;

const { Search } = Input;

const MyIcon = createFromIconfontCN({
  scriptUrl: "//at.alicdn.com/t/font_1712315_wmpnfcq9wg.js"
});

message.config({
  top: 110,
  duration: 1.5
});

const gradient = ` 90deg,
rgba(2, 0, 36, 1) 0%,
rgba(120, 63, 72, 0.8491771708683473) 100%,
rgba(0, 212, 255, 1) 100%`

const StyledContainer = styled.div`
  position: relative;
  margin: 0;
  height:750px;
  margin-top: 40px;
  background: linear-gradient(
   ${gradient}
  );
  box-shadow: 1px 2px 6px -3px rgba(0, 0, 0, 0.75);
  @media screen and (max-width: 1360px) {
   height:auto;
  }
`;
const StyledCardContainer = styled(Row)`
  postion: relative;
  color: white;
  font-size:10px;
  padding:40px 0;
  margin-right:1rem;
  background: linear-gradient(${gradient}
  );
  @media screen and (max-width: 1360px) {
    display:flex;
    flex-direction:column;
  }
`;

const StyledInput = styled(Search)`
  .ant-input {
    color: white;
    background-color: inherit;
    border: none;
    outline: none;
    border-bottom: 2px solid antiquewhite;
    :hover {
      outline: none;
    }
    :focus {
      outline: none;
      border: none;
    }
  }
  .ant-btn {
    border: none;
    outline: none;
    color: black;
    background-color: inherit;
    :hover {
      background-color: #7d332b;
    }
  }
`;

const StyledSearch = styled.div`
margin-right:1rem;
`;

const StyledTitle = styled.div`
  font-size: 4vw;
  color: white;
  font-weight: bold;
`;
const StyledTemperature = styled.div`
  font-size: 6rem;
  font-weight:bolder;
  padding-left: 5px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const StyledSpin = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
`;
const StyledCol = styled(Col)`
  font-size: 2rem;
  border-left: 1px solid white;
  padding-left: 30px;
  @media screen and (max-width: 1360px) {
    border-left:none;
    border-top: 1px solid white;
  }
`;

const StyledIcon = styled.span`
  font-size: 3vw;
  color:white;
`;


type WeatherProps = {};

const Weather: React.SFC<WeatherProps> = () => {
  const [location, setlocation] = useState("KATHMANDU");
  const [temperature, setTemperature] = useState("");
  const [windSpeed, setWindSpeed] = useState("");
  const [visibility, setVisibility] = useState("");
  const [pressure, setPressure] = useState("");
  const [humidity, setHumidity] = useState("");
  const [isDay, setIsDay] = useState(true);
  const [title, setTitle] = useState("");
  const [feels, setFeels] = useState("");
  const [loading, setLoading] = useState(true);
  const [date, setDate] = useState("");
  const [error, setError] = useState(false);

  useEffect(() => {
    const getData = async (location: any) => {
      const appId = "cbce331bc93c9c9a5a88c45c7bb9164e";
      setLoading(true);
      try {
        // fetch data from a url endpoint
        const response = await axios.get(
          `http://api.weatherstack.com/current?access_key=${appId}&query=${location}`
        );
        console.log(response.data);
        setLoading(false);
        setTitle(response.data.location.name);
        setDate(response.data.location.localtime);
        setIsDay(response.data.current.is_day === "yes" ? true : false);
        setTemperature(response.data.current.temperature);
        setWindSpeed(response.data.current.wind_speed);
        setVisibility(response.data.current.visibility);
        setPressure(response.data.current.pressure);
        setFeels(response.data.current.feelslike);
        setHumidity(response.data.current.humidity);
      } catch (error) {
        setError(true);
      }
    };
    getData(location);
  }, [location]);

  return (
    <StyledContainer>
     {loading ? (<StyledSpin>
       <Spin/>
     </StyledSpin>):( <>
      <Row align="middle" gutter={[0,48]}>
        <Col>
          <Row align="middle">
            <Col span={2} offset={1}>
              {" "}
              <StyledIcon>
                {isDay ? (
                  <MyIcon type="icon-weather2" />
                ) : (
                  <MyIcon type="icon-night" />
                )}
              </StyledIcon>
            </Col>
            <Col>
              <StyledTitle>
                {title},{" "}
                <span
                  style={{
                    fontSize: 25,
                    paddingTop: 15,
                    marginRight: 15,
                    opacity: 0.5
                  }}
                >
                  {moment(date).format("MMMM,DD")}
                </span>
              </StyledTitle>
            </Col>
          </Row>
        </Col>
        <Col offset={1}>
          <StyledSearch>
            <StyledInput
              placeholder="Search Location"
              onSearch={(value: any) => setlocation(value)}
              enterButton
            />
            {error && message.error("Invalid Name")}
          </StyledSearch>
        </Col>
      </Row>
      <StyledCardContainer gutter={[0,48]}>
        <Col offset={1}>
          <StyledTemperature>
            {temperature} <span>&#176;</span>C{" "}
            <p
              style={{
                fontSize: 15,
                fontWeight: 400,
                paddingLeft: 10
              }}
            >
              <span> Feels like {feels}</span>
            </p>
          </StyledTemperature>
        </Col>
        <StyledCol>
        <StyledIcon>
                  <MyIcon type="icon-weather_qiangshachenbao" />
                </StyledIcon>{" "}
                Windspeed
                <br />
                {windSpeed} Kmph</StyledCol>
        <StyledCol>
        <StyledIcon>
                  <MyIcon type="icon-weather" />
                </StyledIcon>
                Humidity
                <br />
                {humidity}</StyledCol>
        <StyledCol>
        <StyledIcon>
                  <MyIcon type="icon-weather_yangsha" />
                </StyledIcon>
                Visibility
                <br />
                {visibility}</StyledCol>
          <StyledCol >
          <StyledIcon>
                  <MyIcon type="icon-weathercock" />
                </StyledIcon>
                Pressure
                <br />
                {pressure}
          </StyledCol>
      </StyledCardContainer></>)}
    </StyledContainer>
  );
};

export default Weather;
