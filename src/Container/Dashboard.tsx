import React from 'react'
import styled from "@emotion/styled";


const StyledContainer = styled.div`
padding-top:10px;
box-shadow: 1px 2px 6px -3px rgba(0,0,0,0.75)

`
const StyledContent = styled.div`
min-height:80vh;
background-color:#7da2c630;
padding:40px 20px;
text-align:center;
font-style:italic;
box-shadow: -1px 1px 4px -3px rgba(0, 0, 0, 0.75);


`
 

const Dashboard: React.SFC= () => {
    return ( 
      <StyledContainer>
          <StyledContent>
              <p>WELCOME TO DASHBOARD</p>
              
          </StyledContent>
      </StyledContainer>
     );
}


export default Dashboard;