import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import styled from "@emotion/styled";

import Game from "./Container/Game";
import Dashboard from "./Container/Dashboard";
import Weather from './Container/Weather'

import { DesktopOutlined, PieChartOutlined, AntCloudOutlined } from "@ant-design/icons";

const { Header, Content, Footer, Sider } = Layout;

const StyledLink = styled(Link)`
  color: white;
`;

const StyledFooter = styled(Footer)`
  text-align: center;
`;

const StyledContent = styled(Content)`
  margin: 0 16px;
`;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
  background-color: red;
`;

const App = () => {
  return (
    <Router>
      <StyledLayout>
        <Sider>
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item key="1">
              <StyledLink to="/">
                <PieChartOutlined />
                Home
              </StyledLink>
            </Menu.Item>
            <Menu.Item key="2">
              <StyledLink to="/Game">
                <DesktopOutlined /> Play Game
              </StyledLink>
            </Menu.Item>
            <Menu.Item key="3">
              <StyledLink to="/weather">
              <AntCloudOutlined /> Weather
              </StyledLink>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header />
          <StyledContent>
            <Switch>
              <Route exact path="/">
                <Dashboard />
              </Route>
              <Route path="/Game">
                <Game />
              </Route>
              <Route>
                <Weather/>
              </Route>
            </Switch>
          </StyledContent>
          <StyledFooter>@it-sutra</StyledFooter>
        </Layout>
      </StyledLayout>
    </Router>
  );
};

export default App;
